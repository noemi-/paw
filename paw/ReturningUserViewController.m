//
//  ReturningUserViewController.m
//  LogInTrial
//
//  Created by Noemi Quezada on 8/4/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "ReturningUserViewController.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>

@interface ReturningUserViewController ()


@end

@implementation ReturningUserViewController

@synthesize switchAccount;
@synthesize ContinueAsButton;
@synthesize current_user;
@synthesize profile_picture;
@synthesize background;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //makes the profile image circular
    profile_picture.layer.cornerRadius = profile_picture.frame.size.height/2;
    profile_picture.layer.masksToBounds = YES;//shows the changes on the image frame

    
    // Do any additional setup after loading the view.
    NSLog(@"%@",current_user);
    [ContinueAsButton setTitle:[@"Continue as " stringByAppendingString:current_user.username] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)switch_account:(id)sender
{
    if ([PFUser currentUser])
    {
        [PFUser logOut];
    }
    [self performSegueWithIdentifier:@"SwitchAccount" sender:self];
    
}

/*- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:self {
    
    
    if ([segue.identifier isEqualToString:@"ContinueAs"]) {
        
        
        ReturningUserViewController *returningViewController = segue.destinationViewController;
        
        
        returningViewController.current_user = current_user;
        
        
    }
}*/

-(IBAction)letsSegue:(id)sender
{
    static NSString *tabViewControllerIdentifier = @"TabBarController";
    
    
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:tabViewControllerIdentifier];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIViewController *currentController = app.window.rootViewController;
    app.window.rootViewController = controller;
    app.window.rootViewController = currentController;
    
    [UIView transitionWithView:self.navigationController.view.window
                      duration:0.75
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        app.window.rootViewController = controller;
                    }
                    completion:nil];
}


-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    float yOffset = [UIScreen mainScreen].bounds.size.height <= 480.0f ? 30.0f : 0.0f;
    
    if (yOffset != 0)
    {
        
        [background setImage:[UIImage imageNamed:@"backgroundup1.png"]];
    }
}


@end
