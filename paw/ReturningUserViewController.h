//
//  ReturningUserViewController.h
//  LogInTrial
//
//  Created by Noemi Quezada on 8/4/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ReturningUserViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *switchAccount;
@property (weak, nonatomic) IBOutlet UIButton *ContinueAsButton;
@property (nonatomic, strong) PFUser *current_user;
@property (weak, nonatomic) IBOutlet UIImageView *profile_picture;
@property (weak, nonatomic) IBOutlet UIImageView *background;
-(IBAction)switch_account:(id)sender;
@end
