
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 1
#define COCOAPODS_VERSION_MINOR_AFNetworking 3
#define COCOAPODS_VERSION_PATCH_AFNetworking 4

// GuardPost-ObjectiveC
#define COCOAPODS_POD_AVAILABLE_GuardPost_ObjectiveC
#define COCOAPODS_VERSION_MAJOR_GuardPost_ObjectiveC 0
#define COCOAPODS_VERSION_MINOR_GuardPost_ObjectiveC 1
#define COCOAPODS_VERSION_PATCH_GuardPost_ObjectiveC 1

