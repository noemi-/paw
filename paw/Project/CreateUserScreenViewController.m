//
//  CreateUserScreenViewController.m
//  Project
//
//  Created by Noemi Quezada on 5/29/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//
//Things we need to do
/*
 1. Screen moves up with keyboard DONE
 3. Limiting word word count in text fields DONE
 */
/*#import "CreateUserScreenViewController.h"
#import <GPGuardPost.h>
#import <FacebookSDK/FacebookSDK.h>

#import <CommonCrypto/CommonCrypto.h>
#import <CommonCrypto/CommonKeyDerivation.h>


#import <ctype.h>
#import "AppDelegate.h"





int exists;
int valid;
int email;
int password;
int name;

@interface CreateUserScreenViewController ()



@property (strong, nonatomic) IBOutlet UIView *ContView;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property(weak, nonatomic) IBOutlet UITextField * passwordField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;
@property dispatch_queue_t queue;
@property dispatch_group_t group ;
@property dispatch_queue_t EmailVerificationQueue;
@property dispatch_queue_t EmailQueryQueue;


@property (strong) NSMutableData *user;
@property UITapGestureRecognizer *tapRecognizer;


@end


@implementation CreateUserScreenViewController



-(IBAction)upload // for upload button NEED TO IMPLEMENT
{
    picker2 = [[UIImagePickerController alloc]init];
    picker2.delegate = self;
    [picker2 setSourceType: UIImagePickerControllerSourceTypePhotoLibrary]; //sets the photo library as the image source
    [self presentViewController:picker2 animated: YES completion: NULL];
}
-(IBAction)takePhoto
{
    //Alerts the User that they have no camera to "take a photo"
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"Okay"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    //
    
    picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    [picker setSourceType: UIImagePickerControllerSourceTypeCamera]; //sets the camera as the image source
    [self presentViewController:picker animated: YES completion: NULL];
}
-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    picture = [info objectForKey:UIImagePickerControllerOriginalImage];
    [imageView setImage:picture];//sets the picture the user selects as their picture
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker{ //maybe picker 2
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
    //Look at the textField Tag
    
    switch (textField.tag)
    {
            //Name Field
        case 0:
        {
            
            if(textField.text.length == 0 || [textField.text isEqual: @" "] )
            {
                NSLog(@"missing name");
                [nameIcon setImage:[UIImage imageNamed:@"redName.png"]];
                name = 1;
            }
            else{
                
                [nameIcon setImage:[UIImage imageNamed:@"greenName.png"]];
                name = 0;
            }
            break;
            
            // Email Field
        }
        case 1:
        {
            if (textField.text.length > 0)
            {
                
                _queue= dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_group_t group = dispatch_group_create();
                
                // Add a task to the group
                dispatch_group_async(group, _queue, ^{
                    NSLog(@"Block1");
                    // Send the email address off for validation
                    [GPGuardPost validateAddress:self.emailField.text success:^(BOOL validity, NSString *suggestion)
                     {
                         // Hide the spinner
                         [self.actIndicator stopAnimating];
                         
                         // Update the validity label
                         if(validity) {
                             
                             
                             NSLog(@"valid email");
                             
                             //[emailIcon setImage:[UIImage imageNamed:@"greenEmail.png"]];
                             valid = 0;
                             
                         } else {
                             NSLog(@"invalid email");
                             
                             //[emailIcon setImage:[UIImage imageNamed:@"redEmail.png"]];
                             valid = 1;
                             
                         }
                         
                         // And now check for suggestions:
                         if(suggestion) {
                             NSLog(@"if we wanna give them a suggestion");
                         }
                     } failure:^(NSError *error) {
                         // Hide the spinner
                         [self.actIndicator stopAnimating];
                         NSLog(@"There was an error: %@", [error localizedDescription]);
                         
                         
                     }];
                    
                    /* while(valid == 3)
                     {
                     NSLog(@"Still Block 1");
                     }*/
                    
                   /* NSLog(@"Block1 end");
                });
                
                dispatch_group_async( group, _queue, ^{
                    NSLog(@"Block2");
                    int value = 0;
                    while(valid == 3)
                    {
                        value++;
                        NSLog(@"In Block 2");
                        if (value >200)
                        {
                            
                            //[emailIcon setImage:[UIImage imageNamed:@"redEmail.png"]];
                            valid = 5;
                            break;
                            
                        }
                    }
                    if (valid !=5 && valid != 3)
                    {
                        PFQuery *user = [PFQuery queryWithClassName:@"user"];
                        [user whereKey:@"email" equalTo:self.emailField.text];
                        [user countObjectsInBackgroundWithBlock:^(int count, NSError *error) {
                            if (!error)
                            {
                                
                                if (count > 0)
                                {
                                    NSLog(@"Greater than 0");
                                    [emailIcon setImage:[UIImage imageNamed:@"redEmail.png"]];
                                    exists = 1;
                                    email = 1;
                                }
                                else{
                                    NSLog(@"Less than 0");
                                    [emailIcon setImage:[UIImage imageNamed:@"greenEmail.png"]];
                                    exists = 0;
                                    email = 0;
                                }
                                
                            }
                            else{
                                NSLog(@"Less than 0");
                                [emailIcon setImage:[UIImage imageNamed:@"redEmail.png"]];
                                exists = 1;
                                email = 1;
                            }
                        } ];}
                    
                    
                    
                    
                    NSLog(@"Block2 end");
                });
                
                dispatch_group_notify( group, _queue, ^{
                    NSLog(@"Block3");
                    
                    while (exists == 3 || valid == 3 )
                    {
                    }
                    if (emailIcon.image == [UIImage imageNamed:@"greenEmail.png"])
                    {
                        //[emailIcon setImage:[UIImage imageNamed:@"greenEmail.png"]];
                    }
                    else{
                        
                        [emailIcon setImage:[UIImage imageNamed:@"redEmail.png"]];
                    }
                    
                    
                    
                    NSLog(@"Block3 end");
                });}
            else
            {
                valid = 1;
                exists = 1;
                [emailIcon setImage:[UIImage imageNamed:@"redEmail.png"]];
            }
            
            break;
            
        }
            // Password Field
        case 2:
        {
            
            if(textField.text.length == 0)
            {
                NSLog(@"missing pswd");
                [passwordIcon setImage:[UIImage imageNamed:@"redPswd.png"]];
                password = 0;
            }
            else{
                // Maybe check that it follows the rules of a password if we want to do that
                if ([self PasswordSyntax:self.passwordField.text])
                {
                    
                    // this should go in submit
                    NSUInteger hashedPasswordNumber = [self.passwordField.text hash]; // hash the password
                    
                    // Will have to implement my own keychain wrapper class for this to work
                    hashedPassword = [KeychainWrapper securedSHA256DigestHashForPassword:hashedPasswordNumber];
                    
                    
                    
                    [passwordIcon setImage:[UIImage imageNamed:@"greenPswd.png"]];
                    password = 0;
                }
                else
                {
                    
                    [passwordIcon setImage:[UIImage imageNamed:@"redPswd.png"]];
                    password = 1;
                }
                
                
                
                
            }
            break;
        }
            
    }
}



-(BOOL)PasswordSyntax:(NSString*)password
{
    // if the password is 8 and 10 characters long
    if ([password length] > 7 && [password length] <16)
    {
        for (int i = 0; i < password.length; i++)
        {
            if ([password characterAtIndex:i] != '_' &&
                !isalnum([password characterAtIndex:i]))
            {
                return false;
            }
        }
        return true;
    }
    else{
        return false;
    }
}



-(IBAction)submitButtonPressed:(id)sender
{
    //   NSError *error = nil;
    //NSString *password = self.passwordField.text;
    //NSString *username = self.emailField.text;
    
    
    [self.nameField resignFirstResponder];
    [self.emailField resignFirstResponder]; //bin off the keyboard
    [self.passwordField resignFirstResponder];
    
    [self.actIndicator startAnimating];//starts the spinner
    
    
    if (name ==3 || password == 3 || email == 3)
    {
        [self.actIndicator stopAnimating];//stop the spinner
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please fill out empty fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        // optional - add more buttons:
        //[alert addButtonWithTitle:@"Yes"];
        [alert show];
    }
    else if (name == 1)
    {
        [self.actIndicator stopAnimating];//stop the spinner
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Name field is incorrect" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        // optional - add more buttons:
        //[alert addButtonWithTitle:@"Yes"];
        [alert show];
    }
    else if (exists ==1 && valid == 0)
    {
        [self.actIndicator stopAnimating];//stop the spinner
        NSString *str = [NSString stringWithFormat: @"%@ already has an Account", self.emailField.text];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Already exist" message:str delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        // optional - add more buttons:
        //[alert addButtonWithTitle:@"Yes"];
        [alert show];
    }
    else if (valid == 1 && exists== 0)
    {
        //NSString *str = [NSString stringWithFormat: @"%@ already has an Account", self.emailField.text];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Email" message:@"Email not valid" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        // optional - add more buttons:
        //[alert addButtonWithTitle:@"Yes"];
        [alert show];
    }
    
    else if (password == 1)
    {
        // NSString *str = [NSString stringWithFormat: @"%@ already has an Account", self.emailField.text];
        [self.actIndicator stopAnimating];//stop the spinner
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Password" message:@"Password must be 8 to 16 characters long and be a combination of letters, numbers and underscores" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        // optional - add more buttons:
        //[alert addButtonWithTitle:@"Yes"];
        [alert show];
    }
    else if (name == 0 && password ==  0 && email == 0)
        // if all three are green then
        
    {
        
        // Save the Password hash to the keychain
        if ([KeychainWrapper createKeychainValue:hashedPassword forIdentifier:PASSWORD_SAVED])
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:PASSWORD_SAVED];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_LOGGED_STATUS];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"KEY SAVED SUCCESSULLY TO KEYCHAIN!!");
            
            // Stores name and email of user in Parse Database
            NSLog(@"I am going in!");
            
            PFObject *newUser = [PFObject objectWithClassName:@"user"];
            newUser[@"name"] = self.nameField.text;
            newUser[@"email"] = self.emailField.text;
            [newUser saveInBackground];
            NSLog(@"%@ saved to parse database", self.nameField.text);
            [self.actIndicator stopAnimating];//stop the spinner
            [self letsSegue];
        }
        
        else{
            NSLog(@"KEY NOT SAVED SUCCESSULLY TO KEYCHAIN!!");
            [self.actIndicator stopAnimating];//stop the spinner
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Have an account?" message:@"Seems like you already have an account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            // optional - add more buttons:
            //[alert addButtonWithTitle:@"Yes"];
            [alert show];
            
        }
        
        //Save Email to the keychain
        [[NSUserDefaults standardUserDefaults]setValue:self.emailField.text forKey:EMAIL];
        [[NSUserDefaults standardUserDefaults]synchronize];
        NSLog(@"EMAIL SUCCESSULLY TO KEYCHAIN!!");
        
    }
    /*else{
     NSLog(@"It already exists!");
     [self.actIndicator stopAnimating];//stop the spinner
     
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Have an account?" message:@"Seems like you already have an account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
     // optional - add more buttons:
     //[alert addButtonWithTitle:@"Yes"];
     [alert show];
     }*/
    
    
/*}

-(void)letsSegue
{
    static NSString *tabViewControllerIdentifier = @"TabBarController";
    
    
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:tabViewControllerIdentifier];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIViewController *currentController = app.window.rootViewController;
    app.window.rootViewController = controller;
    app.window.rootViewController = currentController;
    
    [UIView transitionWithView:self.navigationController.view.window
                      duration:0.75
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        app.window.rootViewController = controller;
                    }
                    completion:nil];
}

-(void)resetKeychain {
    [self deleteAllKeysForSecClass:kSecClassGenericPassword];
    [self deleteAllKeysForSecClass:kSecClassInternetPassword];
    [self deleteAllKeysForSecClass:kSecClassCertificate];
    [self deleteAllKeysForSecClass:kSecClassKey];
    [self deleteAllKeysForSecClass:kSecClassIdentity];
}

-(void)deleteAllKeysForSecClass:(CFTypeRef)secClass {
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    [dict setObject:(__bridge id)secClass forKey:(__bridge id)kSecClass];
    OSStatus result = SecItemDelete((__bridge CFDictionaryRef) dict);
    NSAssert(result == noErr || result == errSecItemNotFound, @"Error deleting keychain data (%ld)", result);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self resetKeychain]; // this function resets keychain
    
    // View controller becomes the delegate of the text field
    self.nameField.delegate = self;
    self.emailField.delegate = self;
    self.passwordField.delegate= self;
    
    
    //display the navigation bar
    //[self.navigationController setNavigationBarHidden:NO];
    valid = 3;
    exists=3;
    password = 3;
    email= 3;
    name= 3;
    
    //makes the profile image circular
    imageView.layer.cornerRadius = imageView.frame.size.height/2;
    imageView.layer.masksToBounds = YES;//shows the changes on the image frame
    
    [emailIcon setImage:[UIImage imageNamed:@"greyEmail.png"]];
    [nameIcon setImage:[UIImage imageNamed:@"greyName.png"]];
    [passwordIcon setImage:[UIImage imageNamed:@"greyPswd.png"]];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter]; // Create a notification for the keyboard
    [nc addObserver:self selector:@selector(keyboardWillShow:)name: UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    self.tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTapAnywhere:)];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.emailField || textField == self.nameField || textField == self.passwordField)
    {
        [self animateTextField: textField up: YES];
    }
    
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 180; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if (textField == self.nameField)
    {
        return (newLength > 30) ? NO : YES;
    }
    else if (textField == self.passwordField)
    {
        return (newLength > 15) ? NO : YES;
    }
    else if (textField == self.emailField)
    {
        return (newLength > 35) ? NO : YES;
    }
    else
    {
        return (newLength > 50) ? NO : YES;
    }
}

-(void)keyboardWillShow:(NSNotification *)note
{
    [self.view addGestureRecognizer:self.tapRecognizer];
    
}

-(void)keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:self.tapRecognizer];
}

-(void)didTapAnywhere:(UITapGestureRecognizer*)recognizer{
    if (self.nameField.isEditing)
    {
        [self.nameField resignFirstResponder];
    }
    
    else if (self.passwordField.isEditing)
    {
        [self.passwordField resignFirstResponder];
    }
    
    else if (self.emailField.isEditing)
    {
        [self.emailField resignFirstResponder];
    }
    
}

-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil; // create and set to null
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
        
    }
    return context;
    
}


@end*/
