//
//  AddTaskViewController.h
//  Project
//
//  Created by Maira on 7/4/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"

@interface AddTaskViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *taskNameTextField;
@property (strong) NSManagedObject *taskName;

- (IBAction)doneButtonPressed:(id)sender;
-(IBAction)cancelButtonPressed:(id)sender;


//@property Task *toDoTask;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;

@end
