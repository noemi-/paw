//
//  LogInCustomViewController.m
//  LogInTrial
//
//  Created by Noemi Quezada on 7/24/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "LogInCustomViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface LogInCustomViewController ()

@end

@implementation LogInCustomViewController

@synthesize background; /* view controller background */


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /* Set Background to transparent */
    self.view.backgroundColor = [UIColor clearColor];
    
    /* Get screen size */
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    /* Set background of view controller */
    //create an image
    UIImage * myBackground = [UIImage imageNamed:@"modal.png"];
    
    
    //image view instance to display the image
    self.background = [[UIImageView alloc] initWithImage:myBackground];
    
    //set the frame for the image view
    CGRect myFrame = CGRectMake(0,0, screenWidth,
                                screenHeight);
    [self.background setFrame:myFrame];
    
    //If your image is bigger than the frame then you can scale it
    [self.background setContentMode:UIViewContentModeScaleToFill];
    
    //add the image view to the current view
    [self.logInView addSubview:self.background];
    [self.logInView sendSubviewToBack:self.background];
    
    /*Animate the view */
    [self showAnimate];
    
    
    /* Set Logo Image */
    [self.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]]];
    
    /* Image for closing/x button */
    [self.logInView.dismissButton setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    
    /* Set log in button image */
    [self.logInView.logInButton setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal];
    [self.logInView.logInButton setBackgroundImage:[UIImage imageNamed:@"submitButtonDown2.png"] forState:UIControlStateHighlighted];
    [self.logInView.logInButton setTitle:@"Log in" forState:UIControlStateNormal];
    
    
    /* Change the font size */
    [self.logInView.usernameField setFont:[UIFont systemFontOfSize:14]];

    [self.logInView.passwordField setFont:[UIFont systemFontOfSize:14]];

    [self.logInView.logInButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [self.logInView.passwordForgottenButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
    
    /*Set fields to center */
    self.logInView.usernameField.textAlignment = NSTextAlignmentCenter;
    self.logInView.passwordField.textAlignment = NSTextAlignmentCenter;
    self.logInView.passwordForgottenButton.titleLabel.textAlignment = NSTextAlignmentCenter;

    /* Set forgot password image */
    [self.logInView.passwordForgottenButton setBackgroundImage:[UIImage imageNamed:@"buttonWelcome.png"] forState: UIControlStateNormal];
    [self.logInView.passwordForgottenButton setBackgroundImage:[UIImage imageNamed:@"buttonWelcome.png"] forState: UIControlStateHighlighted];
    
    /* Set forgot password text */
    [self.logInView.passwordForgottenButton setTitle:@"Forgot your username or password?" forState:UIControlStateNormal];
        [self.logInView.passwordForgottenButton setTitle:@"Forgot your username or password?" forState:UIControlStateHighlighted];
    
    /* Set the placeholder text for each textfield */
    /** Password not working properly for some reasons */
    [self.logInView.passwordField setPlaceholder:@"Password"];
    [self.logInView.usernameField setPlaceholder:@"Username"];

    
    /* Set background of each text field */
    self.logInView.usernameField.background = [UIImage imageNamed:@"field.png"];
    self.logInView.passwordField.background = [UIImage imageNamed:@"field.png"];
    
    
    // Set field text color
    [self.logInView.usernameField setTextColor:[UIColor colorWithRed:174.0f/255.0f green:182.0f/255.0f blue:193.0f/255.0f alpha:1.0]];
    [self.logInView.passwordField setTextColor:[UIColor colorWithRed:174.0f/255.0f green:182.0f/255.0f blue:193.0f/255.0f alpha:1.0]];
    [self.logInView.passwordForgottenButton setTitleColor:[UIColor colorWithRed:174.0f/255.0f green:182.0f/255.0f blue:193.0f/255.0f alpha:1.0] forState: UIControlStateNormal];
    [self.logInView.passwordForgottenButton setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:197.0f/255.0f blue:221.0f/255.0f alpha:1.0]forState: UIControlStateHighlighted];

    
    
    // Remove text shadow
    CALayer *layer = self.logInView.usernameField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.logInView.passwordField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.logInView.logInButton.titleLabel.layer;
    layer.shadowOpacity = 0.0f;
    
    self.logInView.logInButton.titleLabel.shadowOffset = CGSizeMake(0, 0);
    self.logInView.passwordForgottenButton.titleLabel.shadowOffset = CGSizeMake(0, 0);
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* Textbox apperance when editing and done editing */

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    /* Highlight text box */
    textField.background = [UIImage imageNamed:@"highlighted-field.png"];
    return YES;
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    /* Highlight text box */
    textField.background = [UIImage imageNamed:@"field.png"];
    return YES;
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    /* Dismiss keyboard */
    [textField resignFirstResponder];
    return YES;
}

/* Animation Stuff Doesn't work as expected */

- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
}


- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        NSLog(@"I came trough");
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

/* When view disappears or will disappear do this */

-(void)viewWillDisappear:(BOOL)animated
{
    [self removeAnimate];
}



/* Sets sizes and locations of items */

- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    
    // Set frame for elements
    /*[self.logInView.dismissButton setFrame:CGRectMake(285.0f, 143.0f, 10.0f, 10.0f)];
    [self.logInView.usernameField setFrame:CGRectMake(35.0f, 210.0f, 250.0f, 40.0f)];
    [self.logInView.passwordField setFrame:CGRectMake(35.0f, 260.0f, 250.0f, 40.0f)];
    [self.logInView.logInButton setFrame:CGRectMake(35.0f, 310.0f, 250.0f, 45.0f)];
    [self.logInView.passwordForgottenButton setFrame:CGRectMake(35.0f, 360.0f, 250.0f, 40.0f)];*/
    
    // for dismiss button offset
    /*float dismissOffset;
    
    // for logo offset and size
    float logosize;
    float logoOffset;
    
    // for forgot password button offset
    float forgotOffset;
    // other offsets and sizes
    float weirdOffset;
    float buttonOffset;
    float weirdsize;
    float weirdbuttonsize;
    float passwordOffset;
    float addition = 5.0f;
    
    // Move all fields down on smaller screen sizes
    float yOffset = [UIScreen mainScreen].bounds.size.height <= 480.0f ? 30.0f : 0.0f;
    
    // as reference for placement
    CGRect fieldFrame = self.logInView.usernameField.frame;
    CGRect logoFrame = self.logInView.logo.frame;
    
    if (yOffset != 0)
    {
        dismissOffset = 20.0f;
        logoOffset = 15.0f;
        logosize = 5.0f;
        forgotOffset = 60.0f;
        weirdOffset = 50.0f;
        buttonOffset = 30.0f;
        weirdsize = 10.0f;
        weirdbuttonsize = 5.0f;
        passwordOffset = 10.0f;
        addition = 0.0f;
    }
    
    // Set frame for elements
    [self.logInView.logo setFrame:CGRectMake(logoFrame.origin.x+25.0f, (logoFrame.origin.y+weirdOffset)+40.0f+logoOffset, (logoFrame.size.width-45.0f)-logosize, (logoFrame.size.height-42.0f)-logosize)];
    
    [self.logInView.dismissButton setFrame:CGRectMake(285.0f, 143.0f - dismissOffset, 10.0f, 10.0f)];
    
    
    
    [self.logInView.usernameField setFrame:CGRectMake((fieldFrame.origin.x)+ logosize,
                                                       fieldFrame.origin.y  +weirdOffset ,
                                                       (fieldFrame.size.width+addition) - weirdsize,
                                                       fieldFrame.size.height-weirdsize)];
    
    
    float offset = fieldFrame.size.height;
    
    [self.logInView.passwordField setFrame:CGRectMake((fieldFrame.origin.x) + logosize,
                                                       ((fieldFrame.origin.y+10.0f)+(weirdOffset-passwordOffset)) + offset,
                                                       (fieldFrame.size.width+addition) - weirdsize,
                                                       fieldFrame.size.height-weirdsize)];

    
    offset += fieldFrame.size.height;
    
    [self.logInView.logInButton setFrame:CGRectMake((fieldFrame.origin.x)+logosize, (fieldFrame.origin.y+20.0f+buttonOffset)+offset, (fieldFrame.size.width+addition)- weirdsize, fieldFrame.size.height - weirdbuttonsize)];
    
    offset += fieldFrame.size.height;
    [self.logInView.passwordForgottenButton setFrame:CGRectMake(35.0f, 360.0f-forgotOffset, 250.0f, 40.0f)];
*/
}
@end

