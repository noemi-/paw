//
//  WelcomeScreenViewController.m
//  Project
//
//  Created by Noemi Quezada on 5/29/14. Please Work 
//  Copyright (c) 2014 Noemi. All rights reserved.
//

/*#import "WelcomeScreenViewController.h"

// Used for saving to NSUserDefaults that a Password has been set, and is the unique identifier for the Keychain.
#define PASSWORD_SAVED @"hasSavedPassword"

// Used for saving to NSUserDefaults that the user is logged in
#define USER_LOGGED_STATUS @"hasLoggedIn"

@interface WelcomeScreenViewController ()

@property (nonatomic) BOOL pinValidated; // tell if user is authenticated
@property   (nonatomic) BOOL userLoggedin; // tell if user was previosly logged in

@end

@implementation WelcomeScreenViewController

//synthesize these things
@synthesize pinValidated;
@synthesize userLoggedin;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /*
    UISwipeGestureRecognizer *Left = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleLeftSwipe:)]; // Initialize a SwipeGestureRecognizer that invokes the handleLeftSwipe function when swiped
    
    Left.direction = UISwipeGestureRecognizerDirectionLeft; // The direction of the gesture is left
    [self.view addGestureRecognizer:Left]; // Add it to the view WelcomeScreen
    */
    
    
    //Initialize when it loads
   /* self.pinValidated = NO;
    self.userLoggedin = NO;
    
    
    //Check user status before loading
    [self CheckUserStatus];
    
    //hide the navigation bar
   // [self.navigationController setNavigationBarHidden:YES];
  
    
    //gradient background
    screenBackground.image = [UIImage imageNamed:@"yellowgradientBKGRD-01.png"];
    
    
    // Load images
    NSArray *imageNames = @[@"helloo.png", @"holaa.png", @"bonjornn.png", @"ciaoo.png",
                            @"gutentagg.png", @"salaamm", @"halloo.png", @"konnichiwaa.png",
                            @"namaskarr.png", @"nihaoo.png", @"olaa.png"]; //sets an array of the image names
    
    NSMutableArray *images = [[NSMutableArray alloc] init]; //puts the images in this array
    for (int i = 0; i < imageNames.count; i++) {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
    }
    
    // Normal Animation
    animationImageView.animationImages = images; //provides the UI Image View with the images to run through
    animationImageView.animationDuration = 14.00; //the time it needs to complete all of those images
    
    [self.view addSubview:animationImageView];
    [animationImageView startAnimating];

    
    
    
}

-(void)CheckUserStatus
{
    BOOL hasPassword = [[NSUserDefaults standardUserDefaults] boolForKey:PASSWORD_SAVED]; //check if user has password
    BOOL isLoggedin = [[NSUserDefaults standardUserDefaults] boolForKey:USER_LOGGED_STATUS]; // check if user is logged in
    
    // if user has password
    if (hasPassword)
    {
        // and... if user was previously logged in
        if (isLoggedin)
        {
            //Create Segue to list and skip log in/sign up screens
        }
        // else user has to log on
    }
    else
    {
        // else user has to sign up 
    }
}
/*
-(void) handleLeftSwipe:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self performSegueWithIdentifier:@"CreateUser" sender:self]; // Once swiped perform the segue indicated by the Identifer Create user
}
*/

/*- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end*/
