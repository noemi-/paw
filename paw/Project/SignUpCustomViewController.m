//
//  SignUpCustomViewController.m
//  LogInTrial
//
//  Created by Noemi Quezada on 7/24/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "SignUpCustomViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface SignUpCustomViewController ()

@end


@implementation SignUpCustomViewController

@synthesize background;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /* Set Background to transparent */
    self.view.backgroundColor = [UIColor clearColor];
    
    /* Get screen size */
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    /* Set background of view controller */
    //create an image
    UIImage * myBackground = [UIImage imageNamed:@"modal.png"];
    
    
    //image view instance to display the image
    self.background = [[UIImageView alloc] initWithImage:myBackground];
    
    //set the frame for the image view
    CGRect myFrame = CGRectMake(0,0, screenWidth,
                                screenHeight);
    [self.background setFrame:myFrame];
    
    //If your image is bigger than the frame then you can scale it
    [self.background setContentMode:UIViewContentModeScaleToFill];
    
    //add the image view to the current view
    [self.signUpView addSubview:self.background];
    [self.signUpView sendSubviewToBack:self.background];
    
    /* Animate the view */
    [self showAnimate];
    
    
    /* Set Logo Image */
    [self.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]]];
    
    /* Image for closing/x button */
    [self.signUpView.dismissButton setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    
    /* Set sign up button image */
    [self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal];
    [self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@"submitButtonDown2.png"] forState:UIControlStateHighlighted];
    [self.signUpView.signUpButton setTitle:@"Sign up" forState:UIControlStateNormal];
    
    /* Change the font size */
    [self.signUpView.usernameField setFont:[UIFont systemFontOfSize:14]];
    [self.signUpView.emailField setFont:[UIFont systemFontOfSize:14]];
    [self.signUpView.passwordField setFont:[UIFont systemFontOfSize:14]];
    [self.signUpView.additionalField setFont:[UIFont systemFontOfSize:14]];
    [self.signUpView.signUpButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    
    /*Set fields to left justified */
    self.signUpView.usernameField.textAlignment = NSTextAlignmentCenter;
    self.signUpView.passwordField.textAlignment = NSTextAlignmentCenter;
    self.signUpView.emailField.textAlignment = NSTextAlignmentCenter;
    
    
    /* Set the placeholder text for each textfield */
    /** Password not working for some reasons */
    [self.signUpView.passwordField setPlaceholder:@"Password"];
    [self.signUpView.usernameField setPlaceholder:@"Username"];
    [self.signUpView.emailField setPlaceholder:@"Email"];
    
    /* Set background of each text field */
    self.signUpView.usernameField.background = [UIImage imageNamed:@"field.png"];
    self.signUpView.passwordField.background = [UIImage imageNamed:@"field.png"];
    self.signUpView.emailField.background = [UIImage imageNamed:@"field.png"];
    
    
    // Set field text color
    [self.signUpView.usernameField setTextColor:[UIColor colorWithRed:174.0f/255.0f green:182.0f/255.0f blue:193.0f/255.0f alpha:1.0]];
    [self.signUpView.passwordField setTextColor:[UIColor colorWithRed:174.0f/255.0f green:182.0f/255.0f blue:193.0f/255.0f alpha:1.0]];
    [self.signUpView.emailField setTextColor:[UIColor colorWithRed:174.0f/255.0f green:182.0f/255.0f blue:193.0f/255.0f alpha:1.0]];
    
    
    // Remove text shadow
    CALayer *layer = self.signUpView.usernameField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.passwordField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.emailField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.additionalField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.signUpButton.titleLabel.layer;
    layer.shadowOpacity = 0.0f;
    
    self.signUpView.signUpButton.titleLabel.shadowOffset = CGSizeMake(0, 0);
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/* Text field Editing stuff */

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    /* Highlight text box */
    textField.background = [UIImage imageNamed:@"highlighted-field.png"];
    return YES;


}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    /* Highlight text box */
    textField.background = [UIImage imageNamed:@"field.png"];
    return YES;

    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    /* Dismiss keyboard */
    [textField resignFirstResponder];
    return YES;
}

/* Animation stuff */

- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
}


- (void)removeAnimate
{
    [UIView animateWithDuration:1.3 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self removeAnimate];
}

/* Frames and positioning of elements */

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // for dismiss button offset
    /*float dismissOffset;
    
    // for logo offset and size
    float logosize;
    float logoOffset;
    
    
    // Move all fields down on smaller screen sizes
    float yOffset = [UIScreen mainScreen].bounds.size.height <= 480.0f ? 30.0f : 0.0f;
    
    // as reference for placement
    CGRect fieldFrame = self.signUpView.usernameField.frame;
    CGRect logoFrame = self.signUpView.logo.frame;
    
    //CGRECt dismissFrame = self.signUpView.dismissButton.frame;
    if (yOffset != 0)
    {
        dismissOffset = 20.0f;
        logoOffset = 15.0f;
        logosize = 5.0f;

    }
    
    // Set frame for elements
    [self.signUpView.logo setFrame:CGRectMake(logoFrame.origin.x+25.0f, logoFrame.origin.y+40.0f+logoOffset, (logoFrame.size.width-45.0f)-logosize, (logoFrame.size.height-42.0f)-logosize)];
        
    [self.signUpView.dismissButton setFrame:CGRectMake(285.0f, 143.0f - dismissOffset, 10.0f, 10.0f)];
        [self.signUpView.signUpButton setFrame:CGRectMake(35.0f, 350.0f, 250.0f, 45.0f)];
    
    
    
    [self.signUpView.usernameField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                       fieldFrame.origin.y ,
                                                       fieldFrame.size.width - 10.0f,
                                                       fieldFrame.size.height-10.0f)];
    
    
    float offset = fieldFrame.size.height;
    
    [self.signUpView.passwordField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                       fieldFrame.origin.y + offset,
                                                       fieldFrame.size.width - 10.0f,
                                                       fieldFrame.size.height-10.0f)];
    
    offset += fieldFrame.size.height;
    
    [self.signUpView.emailField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                         fieldFrame.origin.y + offset,
                                                         fieldFrame.size.width - 10.0f,
                                                         fieldFrame.size.height-10.0f)];
    
    offset += fieldFrame.size.height;
    
    [self.signUpView.signUpButton setFrame:CGRectMake(fieldFrame.origin.x+5.0f, fieldFrame.origin.y+offset, fieldFrame.size.width- 10.0f, fieldFrame.size.height - 5.0f)];*/
}
@end
