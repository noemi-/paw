//
//  AddTaskViewController.m
//  Project
//
//  Created by Maira on 7/4/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "AddTaskViewController.h"
#import "UncompletedTask.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>

@interface AddTaskViewController ()

//@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;


//@property (weak, nonatomic) IBOutlet UISwitch *privacySwitch;
//@property (weak, nonatomic) IBOutlet UISwitch *completionSwitch;
//@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end

@implementation AddTaskViewController
@synthesize taskNameTextField;
@synthesize taskName;

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // self.navigationController.navigationBar.hidden = NO;
    
    // Do any additional setup after loading the view.
    NSLog(@"task name recieved: %@", [self.taskName valueForKey:@"name"]);
    if (self.taskName) {
        [self.taskNameTextField setText:[self.taskName valueForKey:@"name"]];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    if (sender != self.doneButton)
        return;
    if( self.taskNameTextField.text.length >0)
    {
        self.toDoTask = [[Task alloc] init];
        self.toDoTask.taskName = self.taskNameTextField.text;
        self.toDoTask.completed = NO;
    }
    
    
}*/
/*- (IBAction)doneButtonPressed:(id)sender
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
   UncompletedTask *newTask = [NSEntityDescription insertNewObjectForEntityForName:@"UncompletedTask" inManagedObjectContext:context];
    newTask.name = taskNameTextField.text;
    
    //[newTask setValue:self.taskNameTextField.text forKey:@"name"];
    //[newTask setValue: [NSDate date] forKey:@"dateAdded"];
    
    NSLog(@"adding task when done is hit");
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    
}*/
- (IBAction)cancelButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneButtonPressed:(id)sender{
  //  NSManagedObjectContext *context = [self managedObjectContext];
    
    /*
    if(self.taskName)  //update exsisting task
    {
        [self.taskName setValue:self.taskNameTextField.text forKey:@"name"];
    }
    else{
        // Create a new task
        NSManagedObject *newTask = [NSEntityDescription insertNewObjectForEntityForName:@"UncompletedTask" inManagedObjectContext:context];
        [newTask setValue:self.taskNameTextField.text forKey:@"name"];
        NSDate *currentDate= [NSDate date];
        [newTask setValue:currentDate forKey:@"dateAdded"];
        NSLog(@"date that was added to the database: %@  for task: %@", currentDate, taskNameTextField.text);
    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }*/
    
    
    PFObject *addTask = [PFObject objectWithClassName:@"uncompletedTask"];
    addTask[@"taskName"] = self.taskNameTextField.text;
   // NSDate *currentDate= [NSDate date];
   
    //setting expiration data
    ///////////getting the sunday of the week
    NSDate *today = [[NSDate alloc] init];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             
                             initWithCalendarIdentifier:NSGregorianCalendar];
    
    
    
    // Get the weekday component of the current date
    
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit
                                           
                                                       fromDate:today];
    
    
    
    
    /*
     
     Create a date components to represent the number of days to subtract from the current date.
     
     The weekday value for Sunday in the Gregorian calendar is 1, so subtract 1 from the number of days to subtract from the date in question.  (If today is Sunday, subtract 0 days.)
     
     */
    
    NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
    
    [componentsToSubtract setDay: 0 - ([weekdayComponents weekday] - 1)];
    
    
    
    NSDate *beginningOfWeek = [gregorian dateByAddingComponents:componentsToSubtract
                               
                                                         toDate:today options:0] ;
    
    
    /*
     
     Optional step:
     
     beginningOfWeek now has the same hour, minute, and second as the original date (today).
     
     To normalize to midnight, extract the year, month, and day components and create a new date from those components.
     
     */
    
    NSDateComponents *components = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate: beginningOfWeek];
    
    beginningOfWeek = [gregorian dateFromComponents:components];
    NSLog(@"beg of week: %@", beginningOfWeek);
    
    
    NSDateComponents *componentsToAdd = [gregorian components:NSDayCalendarUnit fromDate:beginningOfWeek];
    [componentsToAdd setDay:7];

    NSDate *endOfWeek = [gregorian dateByAddingComponents:componentsToAdd toDate:beginningOfWeek options:0];
   
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, yyyy"];
    
    NSString *myDateString = [formatter stringFromDate:endOfWeek];

    
    NSLog(@"end of week: %@", myDateString);
    
    
    addTask[@"expDate"] = endOfWeek; //storing into database
    /////
    
    
    
    
    [addTask saveInBackground];
    
    
   // [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
 
}
    

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.taskNameTextField])
        [self.taskNameTextField resignFirstResponder];
    return YES;
}
@end
