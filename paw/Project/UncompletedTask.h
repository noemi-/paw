//
//  UncompletedTask.h
//  Project
//
//  Created by Maira on 7/22/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface UncompletedTask : NSManagedObject

@property (nonatomic, retain) NSDate * dateAdded;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) User *userRelationship;

@end
