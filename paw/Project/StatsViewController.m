//
//  StatsViewController.m
//  Project
//
//  Created by Maira on 8/5/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "StatsViewController.h"
#import "WelcomeScreenViewController.h"

@interface StatsViewController ()

@end

@implementation StatsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)logOutPressed:(id)sender
{
    [PFUser logOut];
    NSLog(@"logging out");
    [self performSegueWithIdentifier:@"backToWelcomeScreen" sender:self];
   // [self dismissViewControllerAnimated:YES completion:nil];
   // if (![PFUser currentUser]) {
        
    //    [self.navigationController popToRootViewControllerAnimated:YES];
    //}
  //  [self presentViewController: animated:YES completion:nil];
  //  [self presentViewController:WelcomeScreenViewController animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
