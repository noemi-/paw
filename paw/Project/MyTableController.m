//
//  MyTableController.m
//  ParseStarterProject
//
//  Created by James Yu on 12/29/11.
//

#import "MyTableController.h"
#import "CompletionFormViewController.h"
#import "Task.h"

@implementation MyTableController

- (id)initWithCoder:(NSCoder *)aCoder {
    self = [super initWithCoder:aCoder];
    if (self) {
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"uncompletedTask";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"taskName";
        
        // The title for this table in the Navigation Controller.
        self.title = @"Uncompleted";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
       // self.paginationEnabled = YES;
        
        // The number of objects to show per page
        //self.objectsPerPage = 5;
    }
    return self;
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Not this week"; //Name for the swipe to delete
    //return @"X";
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20; //play around with this value
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ///////setting the beg and end of week
    NSDate *today = [[NSDate alloc] init];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             
                             initWithCalendarIdentifier:NSGregorianCalendar];
    
    // Get the weekday component of the current date
    
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit
                                           
                                                       fromDate:today];
    
    
    NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
    
    [componentsToSubtract setDay: 0 - ([weekdayComponents weekday] - 1)];
    
    
    NSDate *beginningOfWeek = [gregorian dateByAddingComponents:componentsToSubtract
                               
                                                         toDate:today options:0] ;
    
    NSDateComponents *components = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate: beginningOfWeek];
    
    beginningOfWeek = [gregorian dateFromComponents:components];
    //^beginning of week
    
    
    NSDateComponents *componentsToAdd = [gregorian components:NSDayCalendarUnit fromDate:beginningOfWeek];
    [componentsToAdd setDay:7];
    
    NSDate *endOfWeek = [gregorian dateByAddingComponents:componentsToAdd toDate:beginningOfWeek options:0];
    //^end of week
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d"];
    
    
    NSString *weekStartDate = [formatter stringFromDate:beginningOfWeek];
    NSString *weekEndDate = [formatter stringFromDate:endOfWeek];
    
    
    
    NSString *header = weekStartDate;
    header = [header stringByAppendingString:@"  to  "];
    header = [header stringByAppendingString:weekEndDate];
    
    
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    lbl.textAlignment = UITextAlignmentCenter;
  //  lbl.font = [UIFont fontWithName:@"System" size:6];
    lbl.font=[lbl.font fontWithSize:11];
    lbl.text = header;
    lbl.textColor = [UIColor grayColor];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.alpha = 0.9;
    return lbl;
}
#pragma mark - Viewlifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshTable:)
                                                 name:@"refreshTable"
                                            object:nil];
    
    
    NSDate *today = [[NSDate alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             
                             initWithCalendarIdentifier:NSGregorianCalendar];
    
    // Get the weekday component of the current date
    
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit
                                           
                                                       fromDate:today];
    NSLog(@"today: %@", today);
    NSLog(@"weekday component: %@", weekdayComponents);
    /*
     sun = 1
     mon= 2
     tue= 3
     */
   // [self queryParseMethodforDate];
}

- (void)queryParseMethodforDate {
    //something is wrong with the function
    NSLog(@"start query");
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    NSDate *currentDate= [[NSDate alloc]init];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
           
            [query orderByAscending:@"expDate"];
            //  PFObject *object = [PFObject objectWithClassName:@"uncompletedTask"];
            // [PFObject deleteAllInBackground:@[object]
           // [query where:"expDate" lessThan: @(currentDate)];
            NSLog(@"query of the exp dates: %@",  query.getFirstObject);
            NSLog(@"query %@", query );
        }
    }];
}
- (void)refreshTable:(NSNotification *) notification
{
    // Reload the recipes
    [self loadObjects];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refreshTable" object:nil];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //the following code is to refresh the list without having to maually do it
    [self loadObjects];//take out this line once the suer stuff works
    if ([PFUser currentUser])
        [self loadObjects];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Parse

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    // This method is called every time objects are loaded from Parse via the PFQuery
}

- (void)objectsWillLoad {
    [super objectsWillLoad];
    
    // This method is called before a PFQuery is fired to get more objects
}


 // Override to customize what kind of query to perform on the class. The default is to query for
 // all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
 
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if ([self.objects count] == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
        
    }
 
    [query orderByAscending:@"createdAt"];
    return query;
}



 // Override to customize the look of a cell representing an object. The default is to display
 // a UITableViewCellStyleDefault style cell with the label being the first key in the object. 
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    
    static NSString *CellIdentifier = @"listPrototypeCell";
 
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        ////[cell.Mybutton addTarget:self action:@selector(checkboxClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // Configure the cell
    cell.textLabel.text = [object objectForKey:@"taskName"];

 
    return cell;
}
- (IBAction)checkboxClicked:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    // UIImage * unselected = [UIImageimageNamed:@"unmarked.png"];

    
    UIImage * selected = [UIImage imageNamed:@"marked.png"];
    //  [checkBox setImage:unselected forState:UIControlStateNormal];
    
    [btn setImage:selected forState:UIControlStateNormal];
    [btn setImage:selected forState:UIControlStateSelected];
    [btn setImage:selected forState:UIControlStateHighlighted];
    NSLog(@"is selected");
    



    
   // [self performSegueWithIdentifier:@"showCompletionController" sender:self];
    
}

/*
 // Override if you need to change the ordering of objects in the table.
 - (PFObject *)objectAtIndex:(NSIndexPath *)indexPath { 
 return [objects objectAtIndex:indexPath.row];
 }
 */

/*
 // Override to customize the look of the cell that allows the user to load the next page of objects.
 // The default implementation is a UITableViewCellStyleDefault cell with simple labels.
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
 static NSString *CellIdentifier = @"NextPage";
 
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 
 if (cell == nil) {
 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
 }
 
 cell.selectionStyle = UITableViewCellSelectionStyleNone;
 cell.textLabel.text = @"Load more...";
 
 return cell;
 }
 */

#pragma mark - Table view data source
-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    [super setEditing:editing animated:animated];
    
    [self.tableView setEditing:editing animated:YES];
    
    /* optional if we want that plus button there when the list is in edit mode
     if (editing) {
     
     self.addButton.enabled = NO;
     
     } else {
     
     self.addButton.enabled = YES;
     
     }*/
    
    [self.tableView reloadData];
    
}

 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
     return YES;
 }
 


 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
     if (editingStyle == UITableViewCellEditingStyleDelete)
     {
 // Delete the row from the data source
    //need to get the current selection to delete PFObject *task = [PFObject objectWithClassName:@"uncompletedTask"];
  //   [task deleteInBackground];
     // Remove the row from data model
         PFObject *object = [self.objects objectAtIndex:indexPath.row];
         [object deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
         [self loadObjects];
     }];

     //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
     }
  
 }
 

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCompletionController"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
      // CompletionFormViewController *destViewController = segue.destinationViewController;
        
        
        //PFObject *object = [self.objects objectAtIndex:[[self.tableView indexPathForSelectedRow]row]];
        PFObject *object = [self.objects objectAtIndex:indexPath.row];
        NSLog(@"object being passed %@", object);
       // Task *task = [[Task alloc]init];
      //  task.taskName = [object objectForKey:@"taskName"];
        
      //  destViewController.task = task;

        
    }
}

@end
