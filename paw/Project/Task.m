//
//  Task.m
//  Project
//
//  Created by Maira on 7/4/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "Task.h"


@implementation Task

-(void) markAsCompleted: (BOOL) isComplete onDate: (NSDate*)date
{
    self.completed = isComplete; //sets the value of whether it is completed or not
    [self setCompletionDate];
}
- (void)setCompletionDate {
    if (self.completed) {
        self.completionDate = [NSDate date];
    } else {
        self.completionDate = nil; //update to null if the user switches the completion switch off
    }
}

@end
