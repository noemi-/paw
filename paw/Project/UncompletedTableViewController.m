//
//  UncompletedTableViewController.m
//  Project
//
//  Created by Maira on 6/18/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "UncompletedTableViewController.h"
#import "Task.h"
#import "AddTaskViewController.h"
#import "UncompletedTask.h"//the entity file
#import "AddTaskViewController.h"
#import "AppDelegate.h"



@interface UncompletedTableViewController ()


@end

@implementation UncompletedTableViewController

@synthesize uncompletedTasks;
@synthesize checkBox;

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)checkBoxMarked:(id)sender
{
     UIButton *btn = (UIButton*)sender;
   // UIImage * unselected = [UIImageimageNamed:@"unmarked.png"];
    
    UIImage * selected = [UIImage imageNamed:@"marked.png"];
  //  [checkBox setImage:unselected forState:UIControlStateNormal];
   
    [btn setImage:selected forState:UIControlStateSelected];
    NSLog(@"is selected");
    [self performSegueWithIdentifier:@"showCompletionController" sender:self];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //hide the navigation bar
    //[self.navigationController setNavigationBarHidden:YES];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Fetch the devices from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"UncompletedTask"];
    self.uncompletedTasks= [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];

    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    [super setEditing:editing animated:animated];
    
    [self.tableView setEditing:editing animated:YES];
    
    /* optional if we want that plus button there when the list is in edit mode
     if (editing) {
     
     self.addButton.enabled = NO;
     
     } else {
     
     self.addButton.enabled = YES;
     
     }*/
    
    [self.tableView reloadData];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.uncompletedTasks.count; //returns the amount of entries in the fetched list of tasks
    //^maybe we should return one more than the count
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ListPrototypeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
 
    // Configure the cell...
    NSManagedObject *task = [self.uncompletedTasks objectAtIndex:indexPath.row];
 
    cell.textLabel.text= [task valueForKey:@"name"];
    

    // Configure the cell...

    /* the checkmark stuff
     if(toDoTasks.completed){
     cell.accessoryType = UITableViewCellAccessoryCheckmark;
     }else{
     cell.accessoryType = UITableViewCellAccessoryNone;
     }*/
    
    return cell;
}



// Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
     // Return NO if you do not want the specified item to be editable.
     return YES;
 }

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete object from database
        [context deleteObject:[self.uncompletedTasks objectAtIndex:indexPath.row]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
            return;
        }
        
        // Remove device from table view
        [self.uncompletedTasks removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // NSString *stringToMove = self.toDoTasks[fromIndexPath.row];
    // [self.toDoTasks removeObjectAtIndex:fromIndexPath.row];
    //[self.toDoTasks insertObject:stringToMove atIndex:toIndexPath.row];
//    NSString *stringToMove = uncompletedTasks[fromIndexPath.row];
//    [uncompletedTasks removeObjectAtIndex:fromIndexPath.row];
//    [uncompletedTasks insertObject:stringToMove atIndex:toIndexPath.row];
    
}*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}*/



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    ///// [self performSegueWithIdentifier:@"goToCompletionPage" sender:self];
    /*
     [tableView deselectRowAtIndexPath:indexPath animated:NO];
     Task *tappedTask = [self.toDoTasks objectAtIndex:indexPath.row];
     tappedTask.completed= !tappedTask.completed;
     [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
     */
}



//- (IBAction)unwindToList:(UIStoryboardSegue *)segue
//{
//    AddTaskViewController *source = [segue sourceViewController];
    //Task *newtask = source.toDoTask;
    /*if(newtask != nil) //checks if done was hit
    {
        //[self.toDoTasks addObject:newtask];
       // [uncompletedTasks addObject:newtask];
        [self.tableView reloadData];
    }*/
   // AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    // Fetching Records and saving it in "fetchedRecordsArray" object
  //  self.fetchedUncompletedListArray = [appDelegate getListofUncompletedTasks];
//    [self.tableView reloadData];
//}
/*- (IBAction)unwindToUncompletedTableViewController:(UIStoryboardSegue *)unwindSegue
{
    
    
}*/

//this changes the word on the swipe to delete
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Not now!";
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCompletionController"]) {
        NSManagedObject *selectedTask = [self.uncompletedTasks objectAtIndex:[[self.tableView indexPathForSelectedRow] row]];
        AddTaskViewController *destViewController = segue.destinationViewController;
        destViewController.taskName = selectedTask;
        NSLog(@"task name sent: %@", selectedTask);
        
    }
}


@end
