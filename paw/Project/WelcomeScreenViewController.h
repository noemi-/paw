//
//  WelcomeScreenViewController.h
//  LogInTrial
//
//  Created by Noemi Quezada on 7/23/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface WelcomeScreenViewController : UIViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *log_in_button;
@property (strong, nonatomic) IBOutlet UIButton *sign_up_button;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

-(IBAction)sign_up:(id)sender;
-(IBAction)log_in:(id)sender;

@end
