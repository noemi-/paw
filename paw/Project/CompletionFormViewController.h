//
//  CompletionFormViewController.h
//  Project
//
//  Created by Maira on 8/1/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Task.h"

@interface CompletionFormViewController : UIViewController <UIImagePickerControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate>
{

    UIImage *chosenPicture;

}
- (IBAction)shareClicked:(id)sender;
-(IBAction)tweetClicked:(id)sender;
- (IBAction)facebookClicked:(id)sender;
-(IBAction)pictureButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *privacySetting;
@property (weak, nonatomic) IBOutlet UITextField *taskNameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *taskImage;
- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)completedButtonPressed:(id)sender;
-(void)takePicture;
-(void)uploadPicture;


@property (nonatomic, strong) Task *task;



@end
