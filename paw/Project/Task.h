//
//  Task.h
//  Project
//
//  Created by Maira on 7/4/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject

@property NSString *taskName;
@property BOOL completed;
@property BOOL privacy;
//need a picture variable
@property NSDate *creationDate;
@property NSDate *completionDate;
@property NSInteger shareValue;

-(void) markAsCompleted: (BOOL) isComplete onDate: (NSDate*)date;
@end
