//
//  SignUpCustomViewController.h
//  LogInTrial
//
//  Created by Noemi Quezada on 7/24/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <Parse/Parse.h>


@interface SignUpCustomViewController : PFSignUpViewController <UITextFieldDelegate>

/* Background for each textfield */
@property (nonatomic, strong) UIImageView *background;

@end
