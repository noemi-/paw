//
//  MyTableController.h
//  ParseStarterProject
//
//  Created by James Yu on 12/29/11.
//

#import <Parse/Parse.h>

@interface CompletedTableViewController : PFQueryTableViewController
{
    NSIndexPath *selectedCellIndexPath;
    UILabel *nameLabel;
    UILabel *weekdayLabel;
    UIView *grayBar;
}

@end
