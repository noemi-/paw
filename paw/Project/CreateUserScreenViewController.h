//
//  CreateUserScreenViewController.h
//  Project
//
//  Created by Noemi Quezada on 5/29/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//
#import <Parse/Parse.h>
#import <UIKit/UIKit.h>
#import <sys/semaphore.h>
#import <dispatch/dispatch.h>
#import <dispatch/queue.h>

extern int exists;
extern int valid;
extern int email;
extern int password;
extern int name; 


@interface CreateUserScreenViewController : UIViewController <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>{

    UIImagePickerController *picker;
    UIImagePickerController *picker2;
    UIImage *picture;
   
    IBOutlet UIImageView *imageView;
    IBOutlet UIImageView *nameIcon;
    IBOutlet UIImageView *emailIcon;
    IBOutlet UIImageView *passwordIcon;
    
    NSString *hashedPassword;
}

-(IBAction)upload;
-(IBAction)takePhoto;
-(IBAction)submitButtonPressed:(id)sender;
-(void)keyboardWillShow:(NSNotification *)note; // what will happen if keyboard will show
-(void)keyboardWillHide:(NSNotification *) note; // what will happen if keyboard needs


@end
