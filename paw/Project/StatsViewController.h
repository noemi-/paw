//
//  StatsViewController.h
//  Project
//
//  Created by Maira on 8/5/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "WelcomeScreenViewController.h"

@interface StatsViewController : UIViewController
- (IBAction)logOutPressed:(id)sender;

@end
