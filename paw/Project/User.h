//
//  User.h
//  Project
//
//  Created by Maira on 7/22/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UncompletedTask;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * motto;
@property (nonatomic, retain) NSData * userImage;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *uncompletedRelationship;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addUncompletedRelationshipObject:(UncompletedTask *)value;
- (void)removeUncompletedRelationshipObject:(UncompletedTask *)value;
- (void)addUncompletedRelationship:(NSSet *)values;
- (void)removeUncompletedRelationship:(NSSet *)values;

@end
