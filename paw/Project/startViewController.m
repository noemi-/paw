//
//  startViewController.m
//  LogInTrial
//
//  Created by Noemi Quezada on 8/3/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "startViewController.h"
#import "ReturningUserViewController.h"
#import <Parse/Parse.h>

@interface startViewController ()

@end

@implementation startViewController
PFUser *current_user;
@synthesize background;
@synthesize LoadingIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
    
}


- (void)viewDidAppear:(BOOL)animated {
    
    [LoadingIndicator startAnimating];//starts the spinner
    
    current_user = [PFUser currentUser];
    
    UIStoryboardSegue *segue = [[UIStoryboardSegue alloc]init];
    
    [self prepareForSegue:segue sender:self];
    
    // If user is not logged in
    if (!current_user)
    {
        [LoadingIndicator stopAnimating];//stop the spinner
        
        [self performSegueWithIdentifier:@"ToLogSign" sender:self];
        
        
    }
    else{
        
        [LoadingIndicator stopAnimating];//stop the spinner
        
        [self performSegueWithIdentifier:@"ContinueAs" sender:self];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:self {
    
    
    if ([segue.identifier isEqualToString:@"ContinueAs"]) {
        
        
         ReturningUserViewController *returningViewController = segue.destinationViewController;
        
        
        returningViewController.current_user = current_user;

        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    float yOffset = [UIScreen mainScreen].bounds.size.height <= 480.0f ? 30.0f : 0.0f;
    
    if (yOffset != 0)
    {
        
        [background setImage:[UIImage imageNamed:@"backgroundup1.png"]];
    }
}

@end
