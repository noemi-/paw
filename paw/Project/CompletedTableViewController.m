//
//  MyTableController.m
//  ParseStarterProject
//
//  Created by James Yu on 12/29/11.
//

#import "CompletedTableViewController.h"



@implementation CompletedTableViewController


- (id)initWithCoder:(NSCoder *)aCoder {
    self = [super initWithCoder:aCoder];
    if (self) {
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"CompletedTask";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"taskName";
        
        // The title for this table in the Navigation Controller.
        self.title = @"Completed";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
     //    self.paginationEnabled = YES;
        
        // The number of objects to show per page
     //   self.objectsPerPage = 10;
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshTable:)
                                                 name:@"refreshTable"
                                               object:nil];
}

- (void)refreshTable:(NSNotification *) notification
{
    // Reload the recipes
    [self loadObjects];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refreshTable" object:nil];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(selectedCellIndexPath != nil
       && [selectedCellIndexPath compare:indexPath] == NSOrderedSame)
    {
        //return 280;
        return 355;
    }
    
    //return 140;
    return 208;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // [tableView beginUpdates];

    
    //selectedCellIndexPath = indexPath;
    //[super tableView:tableView didSelectRowAtIndexPath:indexPath];
    NSIndexPath *previousSelectedIndexPath = selectedCellIndexPath;  // <- save previously selected cell
    selectedCellIndexPath = indexPath;
    
    if (previousSelectedIndexPath) { // <- reload previously selected cell (if not nil)
        
       
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousSelectedIndexPath]
                         withRowAnimation:UITableViewRowAnimationAutomatic];
    }

    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:selectedCellIndexPath]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
    
    
   // [tableView endUpdates];
    
    


    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Parse

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    // This method is called every time objects are loaded from Parse via the PFQuery
}

- (void)objectsWillLoad {
    [super objectsWillLoad];
    
    // This method is called before a PFQuery is fired to get more objects
}


// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if ([self.objects count] == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByDescending:@"completedOn"];
    
    return query;
}



// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the first key in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *CellIdentifier = @"Cell";
    NSIndexPath* indexPathSelected = selectedCellIndexPath;
    UITableViewCell *cell;
    
    
    
    if ( nil == indexPathSelected || [indexPathSelected compare: indexPath] != NSOrderedSame )
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        PFFile *thumbnail = [object objectForKey:@"taskImage"];
        PFImageView *thumbnailImageView = (PFImageView*)[cell viewWithTag:100];
        
        
        // thumbnailImageView.frame = CGRectMake(10, 10, 298, 151);
        thumbnailImageView.image = [UIImage imageNamed:@"testSpash.jpg"];//file name change to placeholder image
        
        thumbnailImageView.file = thumbnail;
        [thumbnailImageView setContentMode:UIViewContentModeScaleAspectFill];
        [thumbnailImageView setClipsToBounds:YES];
        [thumbnailImageView loadInBackground];
        
        
        nameLabel = (UILabel*) [cell viewWithTag:101];
        weekdayLabel = (UILabel*) [cell viewWithTag:103];
    //    grayBar = (UIView*) [cell viewWithTag:104];
        
        
        nameLabel.text = [object objectForKey:@"taskName"];
        
        nameLabel.hidden= NO;
        weekdayLabel.hidden=NO;
    //    grayBar.hidden=NO;
        
        //weekdayLabel.text = [object objectForKey:@"completedOn"];
        
        NSDate *today = [[NSDate alloc] init];
        today = [object objectForKey:@"completedOn"];
        NSCalendar *gregorian = [[NSCalendar alloc]
                                 
                                 initWithCalendarIdentifier:NSGregorianCalendar];
        
        // Get the weekday component of the current date
        
        NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit
                                               
                                                           fromDate:today];
        /*
         sun = 1
         mon= 2
         tue= 3
         */

       
        switch (weekdayComponents.weekday) {
     //       case 0:
     //           weekdayLabel.text = @"SATURDAY";
     //           break;
            case 1:
                weekdayLabel.text = @"SUNDAY";
                break;
            case 2:
                weekdayLabel.text = @"MONDAY";
                break;
            case 3:
                weekdayLabel.text = @"TUESDAY";
                break;
            case 4:
                weekdayLabel.text = @"WEDNESDAY";
                break;
            case 5:
                weekdayLabel.text = @"THURSDAY";
                break;
            case 6:
                weekdayLabel.text = @"FRIDAY";
                break;
            case 7:
                weekdayLabel.text = @"SATURDAY";
                break;
            default:
                weekdayLabel.text = @"";
                break;
        }
        

    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        PFFile *thumbnail = [object objectForKey:@"taskImage"];
        PFImageView *thumbnailImageView = (PFImageView*)[cell viewWithTag:100];
        thumbnailImageView.image = [UIImage imageNamed:@"testSpash.jpg"];//file name change to placeholder image
      //  thumbnailImageView.frame = CGRectMake(10, 10, 298, 298);
        thumbnailImageView.file = thumbnail;
        [thumbnailImageView setContentMode:UIViewContentModeScaleAspectFit];
      //  [thumbnailImageView setClipsToBounds:YES];
        [thumbnailImageView loadInBackground];
       
        
        nameLabel = (UILabel*) [cell viewWithTag:101];
        weekdayLabel = (UILabel*) [cell viewWithTag:103];
        grayBar = (UIView*) [cell viewWithTag:104];
        
        weekdayLabel.hidden= YES;
        nameLabel.hidden= YES;
    //    grayBar.hidden=YES;
        // nameLabel.text = [object objectForKey:@"taskName"];
    }
    
    
    
      // Configure the cell
   // cell.textLabel.text = [object objectForKey:@"taskName"];
  //  cell.imageView = [object objectForKey:@"taskImage"];
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"Priority: %@", [object objectForKey:@"priority"]];
    
    return cell;
}


/*
 // Override if you need to change the ordering of objects in the table.
 - (PFObject *)objectAtIndex:(NSIndexPath *)indexPath {
 return [objects objectAtIndex:indexPath.row];
 }
 */

/*
 // Override to customize the look of the cell that allows the user to load the next page of objects.
 // The default implementation is a UITableViewCellStyleDefault cell with simple labels.
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
 static NSString *CellIdentifier = @"NextPage";
 
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 
 if (cell == nil) {
 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
 }
 
 cell.selectionStyle = UITableViewCellSelectionStyleNone;
 cell.textLabel.text = @"Load more...";
 
 return cell;
 }
 */

#pragma mark - Table view data source

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate




@end
