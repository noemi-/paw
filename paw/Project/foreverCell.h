//
//  foreverCell.h
//  Project
//
//  Created by Maira on 8/7/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface foreverCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *parseImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;

@end
