//
//  WelcomeScreenViewController.h
//  Project
//
//  Created by Noemi Quezada on 5/29/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//
#import<Parse/Parse.h>
#import <UIKit/UIKit.h>

@interface WelcomeScreenViewController : UIViewController{
    IBOutlet UIImageView *animationImageView;
    IBOutlet UIImageView * screenBackground;
    __weak IBOutlet UIButton *signUpButton;
    __weak IBOutlet UIButton *logInButton;
}

@end
