//
//  ForeverViewController.h
//  Project
//
//  Created by Maira on 8/7/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "foreverCell.h"

@interface ForeverViewController : UIViewController
{

    NSArray *imageFilesArray;
    NSMutableArray *imagesArray;
}

@property (weak, nonatomic) IBOutlet UICollectionView *imagesCollection;
@end
