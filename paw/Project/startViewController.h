//
//  startViewController.h
//  LogInTrial
//
//  Created by Noemi Quezada on 8/3/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface startViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *LoadingIndicator;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@end
