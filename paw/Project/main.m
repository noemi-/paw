//
//  main.m
//  Project
//
//  Created by Noemi Quezada on 5/29/14.
//  Copyright (c) 2014 Noemi. All rights reserved. Hello Maira!
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
