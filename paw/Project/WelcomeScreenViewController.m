//
//  WelcomeScreenViewController.m
//  LogInTrial
//
//  Created by Noemi Quezada on 7/23/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "WelcomeScreenViewController.h"
#import "SignUpCustomViewController.h"
#import "LogInCustomViewController.h"
#import <Parse/Parse.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "AppDelegate.h"


@interface WelcomeScreenViewController ()



@end

@implementation WelcomeScreenViewController

@synthesize backgroundImage;
@synthesize log_in_button;
@synthesize sign_up_button;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)sign_up:(id)sender
{
    
    // Create the sign up view controller
    PFSignUpViewController *signUpViewController = [[SignUpCustomViewController alloc] init];
    [signUpViewController setDelegate:self]; // Set ourselves as the delegate
    [signUpViewController setFields:PFSignUpFieldsSignUpButton | PFSignUpFieldsUsernameAndPassword | PFSignUpFieldsEmail |PFSignUpFieldsDismissButton];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    // Present the sign up view controller
    //signUpViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:signUpViewController animated:NO  completion:NULL];
    
  
    
}



// Sent to the delegate to determine whether the sign up request should be submitted to the server.
- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    BOOL informationComplete = YES;
    
    // loop through all of the submitted data
    for (id key in info) {
        NSString *field = [info objectForKey:key];
        if (!field || field.length == 0) { // check completion
            informationComplete = NO;
            break;
        }
    }
    
    // Display an alert if a field wasn't completed
    if (!informationComplete) {
        [[[UIAlertView alloc] initWithTitle:@"Missing Information"
                                    message:@"Make sure you fill out all of the information!"
                                   delegate:nil
                          cancelButtonTitle:@"ok"
                          otherButtonTitles:nil] show];
    }
    
    return informationComplete;
}

// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    [signUpController  viewWillDisappear:NO];
    [self dismissModalViewControllerAnimated:NO]; // Dismiss the PFSignUpViewController
    [self letsSegue];
    
}

// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error {
    NSLog(@"Failed to sign up...");
}

// Sent to the delegate when the sign up screen is dismissed.
- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
    [signUpController viewWillDisappear:NO];
    //[self dismissModalViewControllerAnimated:NO];
    NSLog(@"User dismissed the signUpViewController");
}




-(IBAction)log_in:(id)sender
{
    // Create the log in view controller
    PFLogInViewController *logInViewController = [[LogInCustomViewController alloc] init];
    [logInViewController setDelegate:self]; // Set ourselves as the delegate
    [logInViewController setFields: PFLogInFieldsLogInButton| PFLogInFieldsUsernameAndPassword | PFLogInFieldsPasswordForgotten | PFLogInFieldsDismissButton];
    
     self.modalPresentationStyle = UIModalPresentationCurrentContext;
    // Present the log in view controller
    [self presentViewController:logInViewController animated:NO completion:NULL];
}

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [logInController viewWillDisappear:NO];
    [self dismissViewControllerAnimated:NO completion:NULL];
    [self letsSegue];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in...");
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    [logInController viewWillDisappear:NO];
    [self.navigationController popViewControllerAnimated:NO];
}




// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length != 0 && password.length != 0) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:@"Missing Information!!"
                                message:@"Make sure you fill out all of the information!"
                               delegate:nil
                      cancelButtonTitle:@"ok"
                      otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

-(void)letsSegue
{
    static NSString *tabViewControllerIdentifier = @"TabBarController";
    
    
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:tabViewControllerIdentifier];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIViewController *currentController = app.window.rootViewController;
    app.window.rootViewController = controller;
    app.window.rootViewController = currentController;
    
    [UIView transitionWithView:self.navigationController.view.window
                      duration:0.75
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        app.window.rootViewController = controller;
                    }
                    completion:nil];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    float yOffset = [UIScreen mainScreen].bounds.size.height <= 480.0f ? 30.0f : 0.0f;
    
    // as reference for placement
    CGRect logbutton = log_in_button.frame;
    CGRect signbutton = sign_up_button.frame;
    
    //log in button offset
    float logOffset;
    
    if (yOffset != 0)
    {
        logOffset = 40.0f;
        [backgroundImage setImage:[UIImage imageNamed:@"backgroundup1.png"]];
    }
    
    [log_in_button setFrame:CGRectMake(logbutton.origin.x, logbutton.origin.y-logOffset, logbutton.size.width, logbutton.size.height)];
    [sign_up_button setFrame:CGRectMake(signbutton.origin.x, signbutton.origin.y+yOffset, signbutton.size.width , signbutton.size.height)];
}

@end
