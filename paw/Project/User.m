//
//  User.m
//  Project
//
//  Created by Maira on 7/22/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "User.h"
#import "UncompletedTask.h"


@implementation User

@dynamic email;
@dynamic firstName;
@dynamic image;
@dynamic lastName;
@dynamic motto;
@dynamic userImage;
@dynamic username;
@dynamic uncompletedRelationship;

@end
