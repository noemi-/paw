//
//  UncompletedTask.m
//  Project
//
//  Created by Maira on 7/22/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import "UncompletedTask.h"
#import "User.h"


@implementation UncompletedTask

@dynamic dateAdded;
@dynamic name;
@dynamic userRelationship;

@end
