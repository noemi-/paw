//
//  UncompletedTableViewController.h
//  Project
//
//  Created by Maira on 6/18/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface UncompletedTableViewController : PFQueryTableViewController
//@interface UncompletedTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIButton *checkBox;

@property (strong) NSMutableArray *uncompletedTasks;
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;
- (IBAction)checkBoxMarked:(id)sender;
@end
